package br.edu.unipampa.reconhecimento.processamento;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * Created by Dougl on 15/10/2015.
 */
public class Processador {


    /**
     * Converte uma imagens para tons de cinza.
     * @return imagem cinza
     */
    public Mat getEscalaCinza(Mat imagem) {
        Mat imagemCinza = new Mat();
//        Imgproc.cvtColor(imagem, imagemCinza, Imgproc.COLOR_BGR2GRAY);
        Imgproc.resize(imagemCinza, imagemCinza, new Size(2900, 1300));
        Imgproc.blur(imagemCinza, imagemCinza, new Size(3, 3));
        return imagemCinza;
    }
}
