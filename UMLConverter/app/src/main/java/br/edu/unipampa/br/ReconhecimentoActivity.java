package br.edu.unipampa.br;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

import br.edu.unipampa.br.umlconverter.R;
import br.edu.unipampa.controlador.ControladorReconhecimento;
import br.edu.unipampa.modelo.Modelo;
import br.edu.unipampa.view.CameraActivity;
import br.edu.unipampa.view.MensagemUtil;
import br.edu.unipampa.view.XMIActivity;
import br.edu.unipampa.xmi.XMI;

public class ReconhecimentoActivity extends Activity {

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reconhecimento);
        Intent i= getIntent();
        long addr = i.getLongExtra("myImg", 0);
        Mat tempImg = new Mat();
        Mat img = new Mat();
        try {
            tempImg = new Mat(addr);
            img = tempImg.clone();
        }catch (Exception ex){

        }
        this.setTitle("UML Sketch Recognizer");
        imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageBitmap(converterBitmap(img));
        final ControladorReconhecimento controlador = new ControladorReconhecimento();
        controlador.setImagem(img);

        ReconhecimentoActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                LoadingAsync task = new LoadingAsync();
                task.setControlador(controlador);
                task.execute();
            }
        });
    }

    public Bitmap converterBitmap(Mat imagem){
        Bitmap bmp = Bitmap.createBitmap(imagem.cols(), imagem.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(imagem, bmp);
        return bmp;
    }

    private class LoadingAsync extends AsyncTask<Void, Void, Boolean> {
        private ControladorReconhecimento controlador;
        private ProgressDialog progressDialog;
        private XMI xmi;

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(
                    ReconhecimentoActivity.this, ProgressDialog.THEME_DEVICE_DEFAULT_DARK);
            progressDialog.setMessage("Analisando Diagrama");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            boolean validado = true;

            try {

                MensagemUtil.addMsg(ReconhecimentoActivity.this, "Reconhecendo Elementos...");
                Modelo modelo = controlador.reconhecer("diagrama");
                MensagemUtil.addMsg(ReconhecimentoActivity.this, "Salvando XMI...");
                xmi = controlador.salvarXMI(modelo);
            } catch (Exception ex) {
                MensagemUtil.addMsg(ReconhecimentoActivity.this, "Erro ao reconhecer o diagrama! Tente reconhecer o diagrama novamente.");
                Log.e("Reconhecimento", "Erro ao reconhecer o diagrama: " + ex);
                Intent i = new Intent(getBaseContext(), CameraActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            return validado;
        }

        @Override
        protected void onPostExecute(Boolean validacao) {
            try{

                Intent intent = new Intent(ReconhecimentoActivity.this, XMIActivity.class);
                intent.putExtra("xmi", xmi.gerarXMI());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                progressDialog.dismiss();
            }catch(Exception ex){
                MensagemUtil.addMsg(ReconhecimentoActivity.this, "Erro ao gerar o XMI! Tente reconhecer o diagrama novamente.");
                Intent i = new Intent(getBaseContext(), CameraActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        }

        public void setControlador(ControladorReconhecimento controlador) {
            this.controlador = controlador;
        }


        public void carregarPreferencias() {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ReconhecimentoActivity.this);
            String destinoXMI = sharedPref.getString("destinoXMI", "");
        }
    }
//
//    public static void showImage(final Mat imagem) {
//        ReconhecimentoActivity.camera.runOnUiThread(new Runnable() {
//            public void run() {
//                Dialog builder = new Dialog(CameraActivity.camera);
//                builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                builder.getWindow().setBackgroundDrawable(
//                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
//                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                    @Override
//                    public void onDismiss(DialogInterface dialogInterface) {
//                        //nothing;
//                    }
//                });
//
//                ImageView imageView = new ImageView(CameraActivity.camera);
//                Bitmap bmp = Bitmap.createBitmap(imagem.cols(), imagem.rows(), Bitmap.Config.ARGB_8888);
//                Utils.matToBitmap(imagem, bmp);
//                imageView.setImageBitmap(bmp);
//                builder.addContentView(imageView, new RelativeLayout.LayoutParams(
//                        ViewGroup.LayoutParams.WRAP_CONTENT,
//                        ViewGroup.LayoutParams.WRAP_CONTENT));
//                builder.show();
//            }
//        });

//    }
}
