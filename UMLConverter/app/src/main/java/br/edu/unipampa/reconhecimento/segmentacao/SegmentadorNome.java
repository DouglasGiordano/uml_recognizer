package br.edu.unipampa.reconhecimento.segmentacao;

import android.graphics.Camera;
import android.util.Log;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

import br.edu.unipampa.persistencia.Arquivo;
import br.edu.unipampa.view.CameraActivity;

import static org.opencv.imgproc.Imgproc.getStructuringElement;
import static org.opencv.imgproc.Imgproc.morphologyEx;

/**
 * Created by Dougl on 18/10/2015.
 */
public class SegmentadorNome {
    private Mat extrairContornos(Mat imagem) {

        Imgproc.threshold(imagem, imagem, 0, 255, Imgproc.THRESH_OTSU);
        //Imgproc.Canny(imagem, imagem, 150, 255);
        Arquivo.salvarImagem(imagem, "resultadoNomeThreshold.png");
        return imagem;
    }

    public Mat extrairNome(Mat imagemNome){
        Imgproc.resize(imagemNome, imagemNome, new Size((imagemNome.size().width*1.5), (imagemNome.size().height*1.5)));
        Arquivo.salvarImagem(imagemNome, "resultadoimagemNome.png");
        imagemNome = extrairContornos(imagemNome);
        imagemNome = retirarFundo(imagemNome);

        return imagemNome;
    }

    /**
     * Retira o fundo de uma imagem e pega o conteudo de dentro
     *
     * @param imagem
     * @return
     */
    private Mat retirarFundo(Mat imagem){

    ArrayList<MatOfPoint> contornos = new ArrayList<MatOfPoint>();
        ArrayList<MatOfPoint> contornoTotal = new ArrayList<MatOfPoint>();
        Scalar colorContorno = new Scalar(255, 255, 255);
        Mat hierarquia = new Mat();
        Mat hierarquia2 = new Mat();
        Imgproc.findContours(imagem, contornos, hierarquia, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        Imgproc.findContours(imagem, contornoTotal, hierarquia2, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);
        Mat resultadoBorda = new Mat(imagem.size(), CvType.CV_8UC3, new Scalar(0, 0, 0));
//        Mat teste = new Mat(imagem.size(), CvType.CV_8UC3,new Scalar(0, 0, 0));

//        for(MatOfPoint contorno: contornoTotal){
//            Imgproc.fillConvexPoly(teste, contorno, new Scalar(255,255,255));
//        }
//        CameraActivity.showImage(teste);
        Imgproc.drawContours(resultadoBorda, contornos, -1, colorContorno, -1);
        Mat resultadoTotal = new Mat(imagem.size(), CvType.CV_8UC3,new Scalar(0, 0, 0));
        Imgproc.drawContours(resultadoTotal, contornoTotal, -1, colorContorno,-1);
        Mat resultadoRetiradaFundo = new Mat(imagem.size(), CvType.CV_8UC3, new Scalar(0, 0, 0));
        Core.absdiff(resultadoBorda, resultadoTotal, resultadoRetiradaFundo);
        return resultadoRetiradaFundo;
    }
}
