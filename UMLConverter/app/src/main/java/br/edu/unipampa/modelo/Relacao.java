package br.edu.unipampa.modelo;

/**
 * Created by Douglas Giordano on 14/10/2015.
 */
public class Relacao extends Elemento{
    private Classe classeOrigem;
    private Classe classeDestino;
    private Multiplicidade multiplicidadeOrigem;
    private Multiplicidade multiplicidadeDestino;

    public Classe getClasseOrigem() {
        return classeOrigem;
    }

    public void setClasseOrigem(Classe classeOrigem) {
        this.classeOrigem = classeOrigem;
    }

    public Classe getClasseDestino() {
        return classeDestino;
    }

    public void setClasseDestino(Classe classeDestino) {
        this.classeDestino = classeDestino;
    }

    public Multiplicidade getMultiplicidadeOrigem() {
        return multiplicidadeOrigem;
    }

    public void setMultiplicidadeOrigem(Multiplicidade multiplicidadeOrigem) {
        this.multiplicidadeOrigem = multiplicidadeOrigem;
    }

    public Multiplicidade getMultiplicidadeDestino() {
        return multiplicidadeDestino;
    }

    public void setMultiplicidadeDestino(Multiplicidade multiplicidadeDestino) {
        this.multiplicidadeDestino = multiplicidadeDestino;
    }
}
