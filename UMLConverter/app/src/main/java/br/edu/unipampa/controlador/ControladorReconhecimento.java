package br.edu.unipampa.controlador;

import android.os.Environment;

import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import java.util.ArrayList;

import br.edu.unipampa.modelo.Classe;
import br.edu.unipampa.modelo.Elemento;
import br.edu.unipampa.modelo.Modelo;
import br.edu.unipampa.modelo.Multiplicidade;
import br.edu.unipampa.modelo.Relacao;
import br.edu.unipampa.persistencia.Arquivo;
import br.edu.unipampa.reconhecimento.classificacao.Classificador;
import br.edu.unipampa.reconhecimento.interpretacao.InterpretadorMultiplicidade;
import br.edu.unipampa.reconhecimento.interpretacao.InterpretadorNome;
import br.edu.unipampa.reconhecimento.interpretacao.InterpretadorRelacao;
import br.edu.unipampa.reconhecimento.processamento.Processador;
import br.edu.unipampa.reconhecimento.segmentacao.Segmentador;
import br.edu.unipampa.reconhecimento.segmentacao.SegmentadorNome;
import br.edu.unipampa.xmi.ClassRelation;
import br.edu.unipampa.xmi.Multiplicity;
import br.edu.unipampa.xmi.Relation;
import br.edu.unipampa.xmi.XMI;

/**
 * Created by Dougl on 12/11/2015.
 */
public class ControladorReconhecimento {
    private Mat imagem;
    private Mat imagemColorida;

    private Segmentador segmentador;
    private SegmentadorNome segmentadorNome;

    private InterpretadorRelacao interpretadorRelacao;
    private InterpretadorMultiplicidade interpretadorMultiplicidade;
    private InterpretadorNome interpretadorNome;

    private Processador processador;

    private Classificador classificador;

    public ControladorReconhecimento(){
        this.interpretadorMultiplicidade = new InterpretadorMultiplicidade();
        this.interpretadorNome = new InterpretadorNome();
        this.interpretadorRelacao = new InterpretadorRelacao();

        this.segmentador = new Segmentador();
        this.segmentadorNome = new SegmentadorNome();

        this.processador = new Processador();

        this.classificador = new Classificador();
    }

    public Modelo reconhecer(String nomeDiagrama){
        Imgcodecs.imwrite(Environment.getExternalStorageDirectory().getPath()+"/"+nomeDiagrama+".png", imagem);
        interpretadorNome.iniciarOCR();
        String local = Environment.getExternalStorageDirectory().getPath();
        ArrayList<Elemento> elementos = segmentador.extrairElementos(imagem);
        Modelo modelo = classificador.classificar(elementos, imagem);
        interpretadorRelacao.interpretarConexoes(modelo);
        interpretadorMultiplicidade.interpretarConexoes(modelo);
        for (Classe classe : modelo.getClasses()) {
            Mat nome = new SegmentadorNome().extrairNome(classe.getImagem());
            String nomeTexto = interpretadorNome.interpretarTexto(nome);
            classe.setNome(nomeTexto);
        }
        for(Multiplicidade multiplicidade: modelo.getMultiplicidades()){
            String nomeTexto = interpretadorNome.interpretarTexto(multiplicidade.getImagem());
            multiplicidade.setValor(nomeTexto);
        }

        return modelo;
    }

    public XMI salvarXMI(Modelo modelo){

        XMI xmi = new XMI();
        xmi.getModel().setName("Diagrama");
        for(Classe classe: modelo.getClasses()){
            br.edu.unipampa.xmi.Class classXmi = new  br.edu.unipampa.xmi.Class(classe.getNome());
            for(Relacao relacao : classe.getRelacao()){
                if(relacao.getClasseDestino() != null && relacao.getClasseOrigem() != null) {
                    ClassRelation classRelation = new ClassRelation();
                    classRelation.setNameClass(relacao.getClasseOrigem().getNome());
                    if(relacao.getMultiplicidadeOrigem() != null) {
                        Multiplicity multiplicity = new Multiplicity(relacao.getMultiplicidadeOrigem().getValor(), relacao.getClasseOrigem().getNome());
                        classRelation.setMultiplicityUpper(multiplicity);
                        classRelation.setMultiplicityLower(multiplicity);
                    }

                    ClassRelation classRelation2 = new ClassRelation();
                    classRelation2.setNameClass(relacao.getClasseDestino().getNome());
                    if(relacao.getMultiplicidadeDestino() != null) {
                        Multiplicity multiplicity2 = new Multiplicity(relacao.getMultiplicidadeDestino().getValor(), relacao.getClasseDestino().getNome());
                        classRelation2.setMultiplicityUpper(multiplicity2);
                        classRelation2.setMultiplicityLower(multiplicity2);
                    }
                    Relation relation = new Relation();
                    relation.setClasseOrigem(classRelation);
                    relation.setClasseDestino(classRelation2);

                    classXmi.getRelacoes().add(relation);
                }
            }


            xmi.getModel().getElement().getElements().add(classXmi);
        }
        Arquivo.salvarXMI("Diagrama", xmi);
        return xmi;
    }

    public Mat detectarElementos(){
        return segmentador.localizarElementos(imagem, imagemColorida);
    }

    public void setImagem(Mat imagem){
        this.imagem = imagem;
    }

    public Mat getImagemColorida() {
        return imagemColorida;
    }

    public void setImagemColorida(Mat imagemColorida) {
        this.imagemColorida = imagemColorida;
    }
}
