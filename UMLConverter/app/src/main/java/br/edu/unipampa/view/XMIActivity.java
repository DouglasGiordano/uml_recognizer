package br.edu.unipampa.view;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import br.edu.unipampa.br.umlconverter.R;

public class XMIActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.setTitle("UML Sketch Recognizer");
        this.setTheme(android.R.style.Theme_DeviceDefault);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_xmi);
        TextView tv = (TextView) findViewById(R.id.text);

        Intent i = getIntent();
        String xmi = i.getStringExtra("xmi");
        tv.setText(xmi);
        tv.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.xmi_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }


    private void abrirViewPreferencias() {
        Intent i = new Intent(XMIActivity.this, ConfiguracaoActivity.class);
        startActivity(i);
    }

    private void abrirViewCamera() {
        Intent i = new Intent(getBaseContext(), CameraActivity.class);
        startActivity(i);
    }
}
