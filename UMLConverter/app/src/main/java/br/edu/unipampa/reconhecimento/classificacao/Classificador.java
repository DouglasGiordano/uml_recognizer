package br.edu.unipampa.reconhecimento.classificacao;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

import br.edu.unipampa.modelo.Classe;
import br.edu.unipampa.modelo.Elemento;
import br.edu.unipampa.modelo.Modelo;
import br.edu.unipampa.modelo.Multiplicidade;
import br.edu.unipampa.modelo.Relacao;
import br.edu.unipampa.view.CameraActivity;
import br.edu.unipampa.view.MensagemUtil;

/**
 * Created by Douglas Giordano on 14/10/2015.
 */
public class Classificador {
    public Modelo classificar(ArrayList<Elemento> retangulos, Mat imagem){
        Modelo modelo = new Modelo();
        ArrayList<Classe> classes = new ArrayList<Classe>();
        ArrayList<Relacao> relacoes = new ArrayList<Relacao>();
        ArrayList<Multiplicidade> multiplicidades = new ArrayList<Multiplicidade>();
        for(Elemento elemento: retangulos){
            Point rect_points[] = new Point[4];
            RotatedRect retangulo = elemento.getRetangulo();
            MatOfPoint contorno = elemento.getContorno();
            retangulo.points(rect_points);
            try {
                if (retangulo.size.height / 4 > retangulo.size.width ||
                        retangulo.size.width / 4 > retangulo.size.height) {
                    Relacao relacao = new Relacao();
                    relacao.setRect_points(rect_points);
                    relacao.setImagem(new Mat(imagem, new Rect(rect_points[0], rect_points[2])).clone());
                    relacoes.add(relacao);
                } else if (retangulo.size.area() > 1000 && retangulo.size.width > 50 && retangulo.size.height > 50) {
                    Classe classe = new Classe();
                    classe.setRect_points(rect_points);
                    classe.setImagem(new Mat(imagem, new Rect(rect_points[0], rect_points[2])).clone());
                    classe.setRetangulo(retangulo);
                    classes.add(classe);
                } else if (retangulo.size.area() > 100) {
                    Multiplicidade multiplicidade = new Multiplicidade();
                    multiplicidade.setRect_points(rect_points);
                    multiplicidade.setImagem(new Mat(imagem, new Rect(rect_points[0], rect_points[2])).clone());
                    multiplicidade.setRetangulo(retangulo);
                    multiplicidades.add(multiplicidade);
                }

            }catch(Exception ex){
                MensagemUtil.addMsg(CameraActivity.camera, "Um elemento não pode ser classificado!");
            }
        }
        modelo.setClasses(classes);
        modelo.setRelacoes(relacoes);
        modelo.setMultiplicidades(multiplicidades);
        return modelo;
    }
}
