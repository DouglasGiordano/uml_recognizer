package br.edu.unipampa.xmi;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;

/**
 * Created by Dougl on 22/10/2015.
 */
@XStreamAlias("packagedElement")
public class Class extends Element{
    @XStreamAlias("xmi:id")
    @XStreamAsAttribute()
    private String id;

    @XStreamImplicit(itemFieldName = "ownedMember")
    private ArrayList<Relation> relacoes;

    public Class(String name){
        super();
        this.name = name;
        this.id = name;
        this.type = "uml:Class";
        this.relacoes = new ArrayList<Relation>();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Relation> getRelacoes() {
        return relacoes;
    }

    public void setRelacoes(ArrayList<Relation> relacoes) {
        this.relacoes = relacoes;
    }
}
