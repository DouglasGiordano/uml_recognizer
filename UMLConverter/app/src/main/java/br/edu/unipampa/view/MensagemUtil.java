package br.edu.unipampa.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.widget.Toast;

public class MensagemUtil {

	public static void addMsg(final Activity activity, final String msg) {
		activity.runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
			}
		});

	}

	public static void addMsg(Context context, String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	public static void addMsgOk(Activity activity, String titulo, String msg,
			int icone) {
		AlertDialog.Builder builderDialog = new AlertDialog.Builder(activity);
		builderDialog.setTitle(titulo);
		builderDialog.setMessage(msg);
		builderDialog.setNeutralButton("OK", null);
		builderDialog.setIcon(icone);
		builderDialog.show();
	}

	public static void addMsgConfirm(final Activity activity, final String titulo,
									 final String msg, final int icone, final OnClickListener listener) {
		activity.runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder builderDialog = new AlertDialog.Builder(activity);
				builderDialog.setTitle(titulo);
				builderDialog.setMessage(msg);
				builderDialog.setNegativeButton("Não", null);
				builderDialog.setPositiveButton("Sim", listener);
				builderDialog.setIcon(icone);
				builderDialog.show();
			}
		});

	}

	public static void addMsgConfirm(Context context, String titulo,
			String msg, int icone, OnClickListener listener) {
		AlertDialog.Builder builderDialog = new AlertDialog.Builder(context);
		builderDialog.setTitle(titulo);
		builderDialog.setMessage(msg);
		builderDialog.setNegativeButton("N�o", null);
		builderDialog.setPositiveButton("Sim", listener);
		builderDialog.setIcon(icone);
		builderDialog.show();
	}

}
