/*
 */
package br.edu.unipampa.reconhecimento.interpretacao;


import android.graphics.Bitmap;
import android.os.Environment;
import android.widget.Toast;

import com.googlecode.tesseract.android.TessBaseAPI;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

import java.io.File;

import br.edu.unipampa.persistencia.Arquivo;
import br.edu.unipampa.view.CameraActivity;


/**
 *
 * @author Douglas Giordano
 */
public class InterpretadorNome {
    TessBaseAPI baseApi;

    public InterpretadorNome() {

    }

    public void iniciarOCR(){
        verificarArquivoTreinamento();
        baseApi = new TessBaseAPI();
        String fileName = Environment.getExternalStorageDirectory().getPath() + "/tesseract";
        baseApi.init(fileName, "por");
    }

    public String interpretarTexto(Mat imagem) {
        Bitmap bmp = Bitmap.createBitmap(imagem.cols(), imagem.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(imagem, bmp);
        baseApi.setImage(bmp);
        return baseApi.getUTF8Text();
    }

    public void verificarArquivoTreinamento(){
        String fileName = Environment.getExternalStorageDirectory().getPath() + "/tesseract";
        File file = new File(fileName);
        if(file.exists()){
            return;
        }else {
            Arquivo arquivo = new Arquivo();
            arquivo.copiarArquivosParaSD();
        }
    }

}
