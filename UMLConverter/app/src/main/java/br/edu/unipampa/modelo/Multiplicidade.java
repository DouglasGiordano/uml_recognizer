package br.edu.unipampa.modelo;

/**
 * Created by Dougl on 10/11/2015.
 */
public class Multiplicidade extends Elemento{
    private String valor;

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
