package br.edu.unipampa.reconhecimento.interpretacao;

import android.util.Log;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.lang.reflect.Array;
import java.util.ArrayList;

import br.edu.unipampa.modelo.Classe;
import br.edu.unipampa.modelo.Modelo;
import br.edu.unipampa.modelo.Relacao;
import br.edu.unipampa.view.CameraActivity;

/**
 * Created by Dougl on 10/11/2015.
 */
public class InterpretadorRelacao {
    public Modelo interpretarConexoes(Modelo modelo) {
        ArrayList<Relacao> relacoes = modelo.getRelacoes();
        ArrayList<Classe> classes = modelo.getClasses();
        for (Relacao relacao : relacoes) {
            Point extremidadeRelacao1 = relacao.getRect_points()[0];
            Point extremidadeRelacao2 = relacao.getRect_points()[2];
            double distanciaAnteriorEx1 = 0;
            double distanciaAnteriorEx2 = 0;
            for (Classe classe : classes) {
                double distancia = getProximidade(extremidadeRelacao1,  classe.getRetangulo().center);
                double distancia2 = getProximidade(extremidadeRelacao2,  classe.getRetangulo().center);

                double mediaDistancia = (classe.getRetangulo().size.width + classe.getRetangulo().size.height) / 1.8;
                if(distancia <= mediaDistancia){
                    if(distancia < distanciaAnteriorEx1 || distanciaAnteriorEx1 == 0) {
                        relacao.setClasseOrigem(classe);
                        distanciaAnteriorEx1 = distancia;
                    }
                }

                if(distancia2 <= mediaDistancia){
                    if(distancia2 < distanciaAnteriorEx2 || distanciaAnteriorEx2 == 0) {
                        relacao.setClasseDestino(classe);
                        distanciaAnteriorEx2 = distancia2;
                    }
                }
            }


        }
        verificarClasses(relacoes);
        return modelo;
    }

    public double getProximidade(Point ponto1, Point ponto2){
        double proximidade = Math.sqrt(Math.pow(ponto1.x - ponto2.x, 2) + Math.pow(ponto1.y - ponto2.y, 2));
        return proximidade;
    }

    public void verificarClasses(ArrayList<Relacao> relacoes){
        for(Relacao relacao: relacoes){
            Classe classeOrigem = relacao.getClasseOrigem();
            Classe classeDestino = relacao.getClasseDestino();

            if(classeOrigem !=null){
                classeOrigem.getRelacao().add(relacao);
            }
            if(classeDestino !=null){
                classeDestino.getRelacao().add(relacao);
            }
        }
    }
}
