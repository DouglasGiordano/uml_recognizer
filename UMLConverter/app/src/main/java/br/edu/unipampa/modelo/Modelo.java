package br.edu.unipampa.modelo;

import java.util.ArrayList;

/**
 * Created by Dougl on 15/10/2015.
 */
public class Modelo {
    private ArrayList<Classe> classes;
    private ArrayList<Relacao> relacoes;
    private ArrayList<Multiplicidade> multiplicidades;

    public ArrayList<Classe> getClasses() {
        return classes;
    }

    public void setClasses(ArrayList<Classe> classes) {
        this.classes = classes;
    }

    public ArrayList<Relacao> getRelacoes() {
        return relacoes;
    }

    public void setRelacoes(ArrayList<Relacao> relacoes) {
        this.relacoes = relacoes;
    }

    public ArrayList<Multiplicidade> getMultiplicidades() {
        return multiplicidades;
    }

    public void setMultiplicidades(ArrayList<Multiplicidade> multiplicidades) {
        this.multiplicidades = multiplicidades;
    }
}
