package br.edu.unipampa.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Camera;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import br.edu.unipampa.br.ReconhecimentoActivity;
import br.edu.unipampa.br.umlconverter.R;
import br.edu.unipampa.controlador.ControladorReconhecimento;
import br.edu.unipampa.modelo.Modelo;

public class CameraActivity extends Activity implements CvCameraViewListener2, View.OnTouchListener {
    private static final String TAG = "UMLConverter::Activity";
    public static CameraActivity camera;
    private Mat mResult;

    private ControladorReconhecimento controlador;
    private CameraView mOpenCvCameraView;

    private String nomeDiagrama;

    private boolean reconhecer = false;
    private boolean pausar = false;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");

                    mOpenCvCameraView.enableView();
                    mOpenCvCameraView.setOnTouchListener(CameraActivity.this);
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    public CameraActivity() {
        camera = this;
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("UML Sketch Recognizer");
        this.setTheme(android.R.style.Theme_DeviceDefault);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.camera_view);
        mOpenCvCameraView = (CameraView) findViewById(R.id.activity_camera_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
        mOpenCvCameraView.enableFpsMeter();
        controlador = new ControladorReconhecimento();
    }


    @Override
    public void onPause() {
        super.onPause();
        this.onCreate(null);
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (!OpenCVLoader.initDebug()) {
                Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
                OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
            } else {
                Log.d(TAG, "OpenCV library found inside package. Using it!");
                mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
            }
        }catch (Exception ex){

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        mResult = new Mat(height, width, CvType.CV_8U);
    }

    @Override
    public void onCameraViewStopped() {
        mResult.release();
    }

    @Override
    public Mat onCameraFrame(final CvCameraViewFrame inputFrame) {
            Mat imagem = inputFrame.gray();
            controlador.setImagem(imagem);
            controlador.setImagemColorida(inputFrame.rgba());
            if (!reconhecer) {
                try {
                    mResult = controlador.detectarElementos();
                } catch (CvException ex) {
                    MensagemUtil.addMsg(this, "Ocorreu um problema na pré visualização da imagem processada.");
                    mOpenCvCameraView.disableView();
                }
            } else {
                abrirViewReconhecimento(imagem);
            }
            return mResult;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.camera_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        reconhecer = true;
        return false;
    }

    private void abrirViewPreferencias() {
        Intent i = new Intent(CameraActivity.this, ConfiguracaoActivity.class);
        startActivity(i);
    }

    private void abrirViewReconhecimento(Mat imagem){
        Mat img = imagem;
        long addr = img.getNativeObjAddr();
        Intent intent = new Intent(this, ReconhecimentoActivity.class);
        intent.putExtra( "myImg", addr );
        startActivity( intent );
    }
}

